/**
 * config.go
 *
 * A simple Zettel Notes template maker.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 * Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba@disroot.org>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"os"

	"github.com/pelletier/go-toml"
)

type Config struct {
	ProjectDescription string `toml:"project_description"`
	ContentDirectory   string `toml:"content_directory"`
}

var config *Config

func parseConfig() {
	tree, err := toml.LoadFile("config.toml")

	config = new(Config)

	if err != nil {
		fmt.Println("Failed to load config.toml", err)
		os.Exit(1)
	}

	err = tree.Unmarshal(config)

	if err != nil {
		fmt.Println("Failed to load config.toml", err)
		os.Exit(1)
	}

	if config.ProjectDescription == "" || config.ContentDirectory == "" {
		fmt.Println("Missing content_directory on config.toml")
		os.Exit(1)
	}
}
