# Filename: Makefile
#
# Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# See the FSF All-Permissive License for more details
# <https://spdx.org/licenses/FSFAP.html>.

# Partially forked from: https://github.com/KRMisha/Makefile

########################################################################
#### Variables and settings
########################################################################

# Executable name
EXEC = zettel

GO := go
# https://stackoverflow.com/a/61580420
SRCS ?= $(wildcard *.go)
SRCS += go.mod go.sum
BUILD_DIR_ROOT = build
VERSION = $(shell git describe --tags --always --long --dirty)

# Linker flags
LDFLAGS = -ldflags="-s -w -X main.version=$(VERSION)"

# Target OS detection
ifeq ($(OS),Windows_NT) # OS is a preexisting environment variable on Windows
	OS = windows
else
	UNAME := $(shell uname -s)
	ifeq ($(UNAME),Darwin)
		OS = macos
	else ifeq ($(UNAME),Linux)
		OS = linux
	else
    	$(error OS not supported by this Makefile)
	endif
endif

# OS-specific settings
ifeq ($(OS),windows)
	TARGET = $(EXEC)-$(VERSION).exe
	RM := rmdir /Q /S
else ifeq ($(OS),macos)
	# Mac-specific settings
	TARGET = $(EXEC)-$(VERSION)
	RM := rm -rf
else ifeq ($(OS),linux)
	# Linux-specific settings
	TARGET = $(EXEC)-$(VERSION)
	RM := rm -rf
endif

########################################################################
#### Targets
########################################################################

.PHONY: all
all: $(SRCS)
	@echo version: $(VERSION)
	$(GO) build -v -o $(BUILD_DIR_ROOT)/$(TARGET) $(LDFLAGS)

.PHONY: test
test:
	$(GO) test ./...

.PHONY: clean
clean:
	$(RM) $(BUILD_DIR_ROOT)
