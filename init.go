/**
 * init.go
 *
 * A simple Zettel Notes template maker.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 * Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba@disroot.org>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package main

import (
	"io"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

const (
	CONFIG_CONTENT = `project_description = "My Personal Zettelkasten"
content_directory = "content"`
	FRONT_MATTER = `---
title: TIMESTAMP
date: TODAY
id: TIMESTAMP
tags:
author:
license:
source:
---`
)

func writeToFile(filename, content string) error {
	file, err := os.Create(filename)

	if err != nil {
		return err
	}

	defer file.Close()

	_, err = io.WriteString(file, content)

	if err != nil {
		return err
	}

	return file.Sync()
}

func writeDefaultConfig(directory string) error {
	err := writeToFile(filepath.Join(directory, "config.toml"), CONFIG_CONTENT)

	return err
}

func writeNewNote(directory string) (error, string) {
	t := time.Now()
	re := regexp.MustCompile(`TIMESTAMP`)
	filename := t.Format("20060102150405") + ".md"
	content := re.ReplaceAllString(FRONT_MATTER, t.Format("20060102150405"))
	re = regexp.MustCompile(`TODAY`)
	content = re.ReplaceAllString(content, t.Format("20060102"))
	err := writeToFile(filepath.Join(directory, filename), content)

	return err, filename
}

func initZettelkasten(directory string) error {
	err := os.MkdirAll(directory, 0755)

	if err != nil {
		return err
	}

	err = writeDefaultConfig(directory)

	if err != nil {
		return err
	}

	return nil
}
