# Zettel

A simple [Zettel Notes][zettel-notes]/[Obsidian][obsidian] template maker.

## Usage

### Create a new Zettelkasten

```sh
zettel --init <path>
```

A configuration file will be created in the path specified by the previous command. Be sure to make the appropriate settings to the `config.toml` file.

#### Example

```sh
zettel --init .
```

### Create a new Zettelkasten note

```sh
zettel --new
```

A new markdown file will be created in the working directory, with a timestamp (with a `YYYYMMDDHHMMSS` format) as file name followed by the `.md` extension. It will also include a *front matter* as content.

### Getting help

```sh
zettel --help
```

## Roadmap

- [ ] Add licensing support.
	- [ ] Front matter.
	- [ ] Config file.
	- [ ] Command-line flag parsing.
- [ ] Add author file.
	- [ ] Front matter.
	- [ ] Config file.

## Authors

- [Iván Ruvalcaba](https://www.github.com/ivanruvalcaba).

## License

[GNU Lesser General Public License v3.0 or later][LGPL-3.0-or-later].

## Copyright

Copyright (C) 2023 — Iván Ruvalcaba <ivanruvalcaba@disroot.org>

[zettel-notes]: https://znotes.thedoc.eu.org/
[obsidian]: https://obsidian.md/
[LGPL-3.0-or-later]: https://spdx.org/licenses/LGPL-3.0-or-later.html
