/**
 * main.go
 *
 * A simple Zettel Notes template maker.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 * Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba@disroot.org>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package main

import (
	"flag"
	"fmt"
	"os"
)

const (
	APP_VERSION = "zettel v0.2.4"
	USAGE       = `A simple Zettel Notes template maker.

Usage of zettel:
  -init, --init		Init a new Zettelkasten on a directory
  -n, --new		Init a new Zettelkasten note
  -s, --show		Prints Zettelkasten description
  -v, --version		Prints current program version
  -h, --help		Prints help information
`
)

func main() {
	var (
		versionFlag bool
		newFlag     bool
		showFlag    bool
		initFlag    string
	)

	flag.StringVar(&initFlag, "init", "", "Init a new Zettelkasten on a directory")
	flag.BoolVar(&newFlag, "n", false, "Init a new Zettelkasten note")
	flag.BoolVar(&newFlag, "new", false, "Init a new Zettelkasten note")
	flag.BoolVar(&showFlag, "s", false, "Prints Zettelkasten description")
	flag.BoolVar(&showFlag, "show", false, "Prints Zettelkasten description")
	flag.BoolVar(&versionFlag, "v", false, "Prints current program version")
	flag.BoolVar(&versionFlag, "version", false, "Prints current program version")
	flag.Usage = func() { fmt.Print(USAGE) }

	flag.Parse()

	if initFlag != "" {
		err := initZettelkasten(initFlag)

		if err != nil {
			fmt.Println("Error generating the new Zettelkasten directory", err)
			os.Exit(1)
		}
	}

	if newFlag || showFlag {
		parseConfig()
	}

	if newFlag {
		err, filename := writeNewNote(config.ContentDirectory)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			fmt.Println("Success! A new note file has been created.")
			fmt.Println("The name for the newly created file is:", filename)
		}
	}

	if showFlag {
		fmt.Println(config.ProjectDescription)
	}

	if versionFlag {
		fmt.Println(APP_VERSION)
	}
}
